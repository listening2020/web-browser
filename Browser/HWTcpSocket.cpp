﻿#include "HWTcpSocket.h"
#include <QDebug>
#include <QUrl>

HWTcpSocket::HWTcpSocket(QObject *parent) : QObject(parent)
{
    mTcpServer  = new QTcpServer(this);
    mTcpReceiver  = new QTcpSocket(this);
    auto status = mTcpServer->listen(QHostAddress::LocalHost,13151);
    if(status)
    {
        QObject::connect(mTcpServer,&QTcpServer::newConnection,this,&HWTcpSocket::ReceiveData);
    }
}

HWTcpSocket::~HWTcpSocket()
{
    mTcpServer->close();
    mTcpReceiver->close();
}

void HWTcpSocket::ReceiveData()
{
    qCout<<"new connect";
    mTcpReceiver = mTcpServer->nextPendingConnection();
//    auto ip = mTcpReceiver->peerAddress();
//    auto port = mTcpReceiver->peerPort();
//    auto iport = QString("connect success ip: %1:%2").arg(ip.toString()).arg(port);
//    qDebug() << iport;
    connect(mTcpReceiver,&QTcpSocket::readyRead,[=](){
        auto ip = mTcpReceiver->peerAddress();
        auto port = mTcpReceiver->peerPort();
        auto iport = QString("connect success! ip: %1:%2").arg(ip.toString()).arg(port);
        qCout << iport;
        data = mTcpReceiver->readAll();
        qCout<<"received data"<<data;
        emit ReceiveSignal();
    });
}

QUrl HWTcpSocket::ByteToUrl()
{
    if(!data.isEmpty())
        return QUrl::fromUserInput(data);
    return QUrl();
}
