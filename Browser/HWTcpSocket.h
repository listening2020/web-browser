﻿#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

// 宏定义打印log支持打印文件名-所在行号
#define qCout qDebug()<<"["<<__FILE__<<":"<<__LINE__<<"]"

class HWTcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit HWTcpSocket(QObject *parent = nullptr);
    ~HWTcpSocket();
    void ReceiveData();
    QUrl ByteToUrl();

signals:
    void ReceiveSignal();

private:
    QTcpServer* mTcpServer;
    QTcpSocket* mTcpReceiver;
    QByteArray data;
};

