﻿#include "HWSocket.h"

HWSocket::HWSocket(QObject *parent) : QObject(parent)
    , mTcpSender(new QTcpSocket(this))
{
}

HWSocket::~HWSocket()
{
    CloseConnect();
}

void HWSocket::ReceiveData(QUrl url)
{
    mUrl = url;

    mTcpSender->connectToHost(QHostAddress::LocalHost,13151);
    connect(mTcpSender, &QTcpSocket::connected, this, &HWSocket::SendData);
}

bool HWSocket::SendData()
{
    if(mTcpSender->waitForConnected())
    {
        auto status = mTcpSender->write(mUrl.toEncoded());
        if(status == -1)
        {
            mTcpSender->write(mUrl.toEncoded());
            qDebug()<<"retry write";
        }
        qDebug()<<"mUrl encoded"<<mUrl.toEncoded();

        if(mTcpSender->waitForBytesWritten())
        {
            qDebug()<<"writed data";
            CloseConnect();
        }
        return true;
    }
    return false;
}

void HWSocket::CloseConnect()
{
    mTcpSender->disconnectFromHost();
    if (mTcpSender->state() == QAbstractSocket::UnconnectedState
            || mTcpSender->waitForDisconnected())
    {
        qDebug("Disconnected!");
    }

    mTcpSender->close();
    qDebug() << "CloseConnect";
    emit CloseSignal();
}

void HWSocket::CloseApp()
{
//    connect(this,&HWSocket::CloseSignal,,QCoreApplication::quit);
}
