﻿#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QUrl>

class HWSocket : public QObject
{
    Q_OBJECT
public:
    HWSocket(QObject *parent = nullptr);
    ~HWSocket();
    void ReceiveData(QUrl url);
    bool SendData();
    void CloseConnect();
    void CloseApp();

signals:
    void CloseSignal();

private:
    QTcpSocket* mTcpSender;
    QUrl mUrl;
};

