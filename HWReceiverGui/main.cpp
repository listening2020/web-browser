﻿#include "MainWindow.h"
#include "HWSocket.h"
#include <QApplication>

QUrl commandLineUrlArgument()
{
    const QStringList args = QCoreApplication::arguments();
    for (const QString &arg : args.mid(1)) {
        if (!arg.startsWith(QLatin1Char('-')))
            return QUrl::fromUserInput(arg);
    }
    return QUrl(QStringLiteral("https://www.hw99.com"));
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto url = commandLineUrlArgument();
    HWSocket *hwSocket = new HWSocket();
    hwSocket->ReceiveData(url);
    MainWindow w;
    w.hide();
    QObject::connect(hwSocket,&HWSocket::CloseSignal,&w,[&w](){
        qDebug()<<"quit";
//        a.quit();
        w.close();
    });
    return a.exec();
}
