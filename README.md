# WebBrowser

#### 介绍
基于Qt5.14 基于Chromium 77内核 QWebEngine制作的浏览器。
同时内部可通过tcp socket套接字形式传输信息，实现更新网页的操作。每次传输完信息后，发送者会自动关闭。


#### 使用说明

1.  可作为正常浏览器使用
2.  可通过HWReceiver.exe与浏览器进行通信传输网址，实现浏览器更新网页操作。
浏览器运行时，输入以下形式命令，即可实现
```
HWReceiver.exe baidu.com
```

