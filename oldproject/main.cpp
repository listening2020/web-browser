#include "mainwindow.h"
#include <QApplication>
#include <QLockFile>
#include <QDir>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString path = QDir::temp().absoluteFilePath("HWWebBrowser.lock.tmp");
    QLockFile *lockFile = new QLockFile(path);
    //上锁失败，不能启动
    if (!lockFile ->tryLock(2000))
    {
        return 1;
    }
    MainWindow w;
    w.show();
    return a.exec();
}
