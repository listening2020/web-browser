#include "mainwindow.h"
#include "ui_mainwindow.h"

// Qt
//#include <QNetworkAccessManager>
#include <QWebEngineView>
//#include <QNetworkRequest>
//#include <QNetworkReply>
#include <QDesktopWidget>
#include <QLabel>
#include <QFile>
#include <QDir>
#include <algorithm>

// local
#include "base64.h"

// 从命令行读取网址
QUrl commandLineUrlArgument()
{
    // 返回命令行参数列表
    const QStringList args = QCoreApplication::arguments();
    for (const QString &arg : args.mid(1)) {
        // startsWith()判断是否以某个字符串开头
        // QLatin1Char()用于构造8位字符的QChar
        if (!arg.startsWith(QLatin1Char('-')))
            // 从用户提供的字符串返回有效的url
            return QUrl::fromUserInput(arg);
    }
    // QStringLiteral编译器把常量字符串str直接构造为QString对象，运行时不再需要额外的构造开销
    return QUrl(QStringLiteral("https://www.hw99.com"));
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("HWWebBrowser");
    MoveWindow();

    webView = new QWebEngineView(this);

    auto url = commandLineUrlArgument();

    QLabel *urlLabel = new QLabel(this);
    urlLabel->setText(url.toString());
    statusBar()->addWidget(urlLabel);
    webView->load(url);

    //将视图部件设置为主窗口的中心部件
    setCentralWidget(webView);

    connect(webView, &QWebEngineView::urlChanged, [this,urlLabel](const QUrl &url) {
        urlLabel->setText(url.toString());
            statusBar()->addWidget(urlLabel);
        });
}

MainWindow::~MainWindow()
{
    delete ui;
}

// 将程序显示到副屏
// 获取程序所在屏幕是第几个屏幕 desktop->screenNumber(this);
// 获取所有屏幕总大小 desktop->geometry();
// 获取主屏幕序号 desktop->primaryScreen();
void MainWindow::MoveWindow()
{
    QDesktopWidget *desktop = QApplication::desktop();

    // 获取所有屏幕总个数
    auto screenCount = desktop->screenCount();

    // 屏幕序号默认从0开始
    // 手写屏分辨率1280x800
    for (int i = 0; i < screenCount;i++)
    {
        // 获取程序所在屏幕尺寸
        auto rect = desktop->screenGeometry(i);
        qDebug()<<"Rect:"<<rect<<"width"<<rect.width()<<"h"<<rect.height();
        if((rect.width() == 1280) && (rect.height() == 800))
        {
            this->setGeometry(desktop->screenGeometry(i));
            break;
        }
    }
    // 顶级屏幕全屏
    //    this->showFullScreen();
}

// 若网址无http前缀，则加上
QString MainWindow::AddHttpPrefix(const QString toAddUrl, const QString isOnlineUrl)
{
    QString addedHttpUrl;
    QLabel* urlLabel = new QLabel(this);

    if(toAddUrl.mid(0,4) == "http" && isOnlineUrl == "0")
    {
        urlLabel->setText(toAddUrl);
        statusBar()->addWidget(urlLabel);
        return toAddUrl;
    }
    else if((toAddUrl.mid(0,3) == "www" || toAddUrl.mid(0,9) == "localhost") && isOnlineUrl == "0")
    {
        addedHttpUrl = "http://"+toAddUrl;
        urlLabel->setText(addedHttpUrl);
        statusBar()->addWidget(urlLabel);
    }
    else if(isOnlineUrl != "0")
    {
        qDebug()<<"local path: "<<toAddUrl;
        QString transformedPath;
        // 转换绝对路径，因\斜杠问题导致不能加载成功，需转换为/
        QDir urlPath(toAddUrl);
        if(urlPath.isAbsolute())
        {
            transformedPath = urlPath.absolutePath();
            qDebug()<<"if: tranformed path: " << transformedPath;
        }
        else
        {
            // qt中相对路径非exe所在路径，故转换为绝对路径
            transformedPath = urlPath.fromNativeSeparators(toAddUrl);
            transformedPath = urlPath.cleanPath(transformedPath);

            qDebug()<<"else: after NativeSeparator and cleanPath: " << transformedPath;
            qDebug()<<"else: absolute path: "<<QCoreApplication::applicationDirPath();
            qDebug()<<"else: current path: "<<QDir::currentPath();

            transformedPath = QCoreApplication::applicationDirPath() +'/'+ transformedPath;
            //transformedPath = toAddUrl;
            qDebug()<<"else: tranformed path: " << transformedPath;
        }
        // 本地网页路径
        urlLabel->setText(transformedPath);
        statusBar()->addWidget(urlLabel);
        return transformedPath;
    }
    else
    {
        QString errInfo("参数输入错误");
        qDebug()<<errInfo;
        urlLabel->setText(errInfo);
        statusBar()->addWidget(urlLabel);
    }

    delete urlLabel;
    return addedHttpUrl;
}



