#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

// 前置声明
class QWebEngineView;
class QNetworkAccessManager;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected slots:

private:
    void MoveWindow();
    QString AddHttpPrefix(const QString toAddUrl,const QString isOnlineUrl = "0");


private:
    Ui::MainWindow *ui;
    QWebEngineView *webView;
    QString encodeUrl;
    QString decodedUrl;
};
#endif // MAINWINDOW_H
